// import { menu } from "./modules/menu";
import { edTabs } from "./modules/tabs";

// Slider VanillaJS (https://github.com/ganlanyuan/tiny-slider)
import { tns } from "../../node_modules/tiny-slider/src/tiny-slider.module";
import { options_slider } from "./modules/slider";

import { contactform } from './modules/contactform'
import { menuinicio, mainmenu } from './modules/menus'

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
Vue.use(VueResource);

const vm = new Vue({
  el: '#exsacruz',
  data: {
    path_page: '/',
    path_media: '/assets/',
    toggle: false,
    formSubmitted: false,
    vue: contactform,
    menuinicio,
    mainmenu
  },
  created: function () {
    window.edTabs = edTabs;
    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  mounted: function () {
    const activeMenuItem = containerId => {
      let links = [...document.querySelectorAll(`#${containerId} a`)];
      const curentUrl = document.location.href;
      links.map(link => {
        if (link.href === curentUrl) {
          link.classList.add('active')
        }
      });
    };
    activeMenuItem('vertical-menu');
    // Tiny Slider Active
    const slider = tns(options_slider);
    // Button ir arriba
    // let scrollV = window.scrollY;
    // const goup = document.getElementById("goup");
    // const goup_event = window.addEventListener('scroll', function () {
    //   scrollV = window.scrollY;
    //   if (scrollV > 120) {
    //     goup.classList.remove('fade-out');
    //     goup.classList.add('fade-in');
    //   }
    //   else {
    //     goup.classList.remove('fade-in');
    //     goup.classList.add('fade-out');
    //   }
    //   // console.log(scrollV);
    // });
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('../../mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    menuToggle: function () {
      if (this.toggle == true) {
        this.toggle = false
        // console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        // console.log('ACTIVO')
      }
    }
  }
})