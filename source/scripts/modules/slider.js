export const options_slider = {
  container: '.exsacruz-slider',
  mode: 'gallery',
  controlsText: [
    '<i class="icon-left"></i>',
    '<i class="icon-right"></i>'
  ],
  autoplayText: [
    '<i class="icon-play"></i>',
    '<i class="icon-pause"></i>'
  ],
  speed: 5000,
  autoplay: true,
  controlsContainer: document.querySelector('.my-controls'),
  animateOut: 'zoomOut'
};