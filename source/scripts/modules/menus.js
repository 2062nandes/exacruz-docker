export const mainmenu = [
  {
    title: 'Extintores',
    href: 'extintores.html',
    description: '',
    image: ''
  },
  {
    title: 'Accesorios para hidrantes',
    href: 'accesorios-para-hidrantes.html',
    description: '',
    image: ''
  },
  {
    title: 'Recarga y mantenimiento de extintores',
    href: 'recarga-y-mantenimiento-de-extintores.html',
    description: '',
    image: ''
  },
  {
    title: 'Capacitaciones',
    href: 'capacitaciones.html',
    description: '',
    image: ''
  }
];

export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home',
    wow_delay: '0.3s'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us',
    wow_delay: '0.5s'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact',
    wow_delay: '0.7s'
  }
];
